# Django my_site MySite

Base django application with docker

## Requirements
- docker
- docker-compose

## Installation
1. clone this repository
    `git clone https://gitlab.com/ragutierrez/django_agendame.git`
2. open `agenda_me` folder
    `cd AgendaMe/agenda_me`
3. execute docker-compose to build image and containers
    `docker-compose up --no-start`
4. migrate models 
    `docker-compose run --rm django python manage.py migrate`
4.1. create a superuser
    `docker-compose run --rm django python manage.py createsuperuser`
5. execute docker-compose to start containers
    `docker-compose up -d`
6. that's it happy codding!!!
